'''
Constants file
'''
LOG_FILE = "log_file"
LOG_FORMAT='[%(asctime)s] %(levelname)s [%(funcName)s:%(lineno)s] %(message)s'

SECTION_GENERAL = "general"
SECTION_GOOGLE = "google"

GOOGLE_DOCUMENT = "doc_id"
OUTPUT_DIR = "output_dir"
CLIENT_ID = "client_id"
CLIENT_SECRET = "client_secret"

