#!/usr/bin/env python2.7


'''
This module handles exporting a google document.
'''


import sys, os
import argparse
import ConfigParser
import logging
import httplib2
from  utils import file_handler 
from  utils.constants import CLIENT_ID, CLIENT_SECRET, \
            LOG_FILE, LOG_FORMAT, SECTION_GOOGLE, SECTION_GENERAL, GOOGLE_DOCUMENT, OUTPUT_DIR
from apiclient.discovery import build
from oauth2client.client import OAuth2WebServerFlow

class GdocsManager(object):
    '''
    This class handles the API to manage google documents
    '''

    client_id = None
    client_secret = None
    drive_service = None
    OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'
    REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'


    def __init__(self, initparams):
        self.client_id = initparams[CLIENT_ID]
        self.client_secret = initparams[CLIENT_SECRET]
        self.google_auth()
        self.parameter_validation()
        logging.basicConfig(
            filename=initparams[LOG_FILE],
            level=logging.DEBUG,
            format=LOG_FORMAT)

    def parameter_validation(self):
        '''
        Validate input parameters, show the suitable error message otherwise
        '''

        if (self.client_id is None):
            raise ValueError(
                "A valid Google Client ID needs to be provided.")
        if (self.client_secret is None):
            raise ValueError(
                "A valid Google Client Secret needs to be provided.")
        if (self.drive_service is None):
            raise ValueError(
                "Google Validatin failed, try again.")


    def google_auth(self):
        '''
        This method builds a valid HTTP request,
        including the suitable google credentials.
        To do so, the required oauth token is obtained
        by requesting the user to accept access to his google account
        (i.e., the provided link needs to be copied into a browser)
        '''

        # Run through the OAuth flow and retrieve credentials
        flow = OAuth2WebServerFlow(
            self.client_id, self.client_secret,
            self.OAUTH_SCOPE, redirect_uri=self.REDIRECT_URI)
        authorize_url = flow.step1_get_authorize_url()
        print 'Go to the following link in your browser: ' + authorize_url
        code = raw_input('Enter verification code: ').strip()
        credentials = flow.step2_exchange(code)
        
        # Create an httplib2.Http object and authorize it with our credentials
        http = httplib2.Http()
        http = credentials.authorize(http)
        self.drive_service = build('drive', 'v2', http=http)

    def download_info_from_file(self, file):
        '''
        Return download url for the provided file metainfo
        In case the file is exportable as PDF, that URL will be used.
        Return the raw original file otherwise
        '''
        links = file.get('exportLinks')
        title = file.get('title').replace(' ', '')
        url = links.get('application/pdf') \
            if (links is not None) else file.get('downloadUrl')
        filename = title if (links is None) else title+".pdf"
        return (url, filename)

    def download_and_store(self, url, path):
        '''
        This method downloads the content of the provided URL
        and stores it in the provided path
        '''
        logging.info(u"Storing content from {0} into {1}".format(url, path))
        resp, content = self.drive_service._http.request(url)
        if resp.status == 200:
            logging.info('Status: {0}'.format(resp))
            file_handler.store_content(content, path)
        else:
            logging.error('An error occurred: {0}'.format(resp))



    def export_doc(self, docid, output_dir, authorship_info=""):
        '''
        This method exports the requested google document.
        PDF is tried by default, the original format will be used otherwise
        '''
        logging.info( "Downloading document {0}" .format(docid))

        # Retrieve file metainfo and the download URL on it
        file = self.drive_service.files().get(fileId=docid).execute()
        (download_url, filename) = self.download_info_from_file(file)

        # Store downloaded file into the provided destination path+
        if authorship_info is not "":
            authorship_info += "-"
        self.download_and_store(download_url, os.path.join(output_dir, authorship_info+filename))

        # Return the final path to the downloaded file for informative purposes
        return os.path.join(output_dir, authorship_info+filename)


def parse_config_file(inifile):
    '''
    Parse .ini configuration file at inifile
    '''
    result = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(inifile)
        result = {
            LOG_FILE:
            cfgparser.get(SECTION_GENERAL, LOG_FILE),

            OUTPUT_DIR:
            cfgparser.get(SECTION_GENERAL,OUTPUT_DIR),

            CLIENT_ID:
            cfgparser.get(SECTION_GOOGLE, CLIENT_ID),

            CLIENT_SECRET:
            cfgparser.get(SECTION_GOOGLE, CLIENT_SECRET),
            
            GOOGLE_DOCUMENT:
            cfgparser.get(SECTION_GOOGLE, GOOGLE_DOCUMENT)}
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the configuration file exists and has a valid format"
            .format(inifile))
    return result


def replace_defaults_with_arguments(defaults, arguments):
    '''
    Taking the dictionary defaults as reference,
    replace (or add if necessary) any value for the matching keys in arguments
    '''    
    params = defaults
    for param in arguments:
        if arguments[param] is not None:
            params[param] = arguments[param]

    return params
    
def parse_arguments():
    '''
    This function parses command line parameters
    and load all the necessary information from a config file
    '''

    parser = argparse.ArgumentParser( description = \
        "Export a Google Document and store it as PDF")


    parser.add_argument('-d', '--docid', action = "store",
        dest = GOOGLE_DOCUMENT,
        help = "Google Document Identifier")
    parser.add_argument('-p', '--path', action = "store",
        dest = OUTPUT_DIR,
        help = "Path to the destination  directory")
    parser.add_argument('-f', '--configfile', action = "store",
        dest="inifile",
        help = "Configuration parameters provided inside a .ini file. \
        These parameters will be overwritten by inline parameters")

    args  = parser.parse_args(sys.argv[1:])
    
    cfgparams = {}
    if args.inifile is not None:
        # Load the defaults in the .ini file
        cfgparams = parse_config_file(args.inifile)

    # Replace defaults with the parameters provided in the command line
    parameters = replace_defaults_with_arguments(cfgparams, vars(args))

    return parameters

if __name__ == '__main__':

    ARGS = parse_arguments()

    # Instantiate the gdoc manager with the provided credentials
    MANAGER = GdocsManager(ARGS)
    MANAGER.export_doc(
        docid = ARGS[GOOGLE_DOCUMENT],
        output_dir = ARGS[OUTPUT_DIR])

    print("== DONE! ==")
