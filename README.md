# Google Documents Manager #

> ### Disclaimer: WIP ###
>
> The code in this repo is continous **work in progress** to learn about python and the API to manage google documents.
> Feel free to use some of it, but take always the **master** branch as a reference.
>
> ***Leave your questions and feedback as new [issues](https://bitbucket.org/alozalv/googleapps_utils/issues).***
>
> *** Add this repo as a [git submodule](https://bitbucket.org/alozalv/googleapps_utils/wiki/How%20to%20add%20this%20repo%20as%20git%20submodule) in your project ***
>
> Thanks!

This repository contains a number of modules to handle Google Documents.

Inside the *gdocs* folder, you'll find:

+ *gdocs_manager.py*: GdocsManager class. Made runnable for examplary purposes (see below).
+ *config*: configuration folder. Copy the sample and replace the values on it with your own.
+ *utils*: some utility modules such as constants, or file helpers.

## Export a Google Document ##

You can run *gdocs_manager.py* in order to export a google document as PDF.

In case the selected document is not available in that format, the original file will be returned:

```
>> ./gdocs_manager.py -h                                                                                                                       
usage: gdocs_manager.py [-h] [-d DOC_ID] [-o DESTINATION_DIR] [-f INIFILE]

Export a Google Document and store it as PDF

optional arguments:
  -h, --help            show this help message and exit
  -d DOC_ID, --docid DOC_ID
                        Google Document Identifier
  -o OUTPUT_DIR, --outputdir OUTPUT_DIR
                        Path to the destination file
  -f INIFILE, --configfile INIFILE
                        Configuration parameters provided inside a .ini file.
                        These parameters will be overwritten by inline
                        parameters
```


It is recommended to include all the required parameters in the configuration file (following the model in config/config.ini_sample). In order to get your Google Client ID and secret, check [this link](https://developers.google.com/drive/quickstart-python#step_1_enable_the_drive_api).

Once you run the script, you will be asked to load the provided URL into a browser and copy the auth token back to proceed. After hitting enter, the script will download the chosen document into the provided destination path:

```
>> ./gdocs_manager.py -f config/alozalv.ini
Go to the following link in your browser: https://accounts.google.com/o/oauth2(....)760ci2d.apps.googleusercontent.com&access_type=offline
Enter verification code: 4/Z2D(...)uhQI
== DONE! ==

```

Additional info is traced using *logging*. Provide a path of choice in your configuration file to do so.
